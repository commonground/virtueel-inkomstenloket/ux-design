﻿using alef_graphql_client;
using Microsoft.AspNetCore.Components;
using StrawberryShake;
using System.Threading.Tasks;

namespace Inkomensloket.ComponentLibrary
{
    public partial class Iit
    {
        [Inject] public IAlefGraphQlClientClient graphQl { get; set; }

        public IOperationResult<IIit> Result { get; set; }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                var input = new Invoer__aanvraag__iitInput()
                {
                    Alleenstaand = false,
                    AowLeeftijdBehaald = false,
                    InkomenPerMaand = 1500,
                    OuderDan21 = true,
                    ThuiswonendeKinderen = false,
                    Vermogen = 12000,
                    Woonplaats = "Utrecht"
                };

                Result = await graphQl.IitAsync(input);
                StateHasChanged();
            }
            await base.OnAfterRenderAsync(firstRender);
        }
    }
}
