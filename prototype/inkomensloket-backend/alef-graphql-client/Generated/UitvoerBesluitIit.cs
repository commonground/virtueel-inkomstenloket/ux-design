﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace alef_graphql_client
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class UitvoerBesluitIit
        : IUitvoerBesluitIit
    {
        public UitvoerBesluitIit(
            string rechtBeschrijving, 
            decimal uitTeKerenToeslagBedrag, 
            bool uitTeKerenToeslagBedragSpecified)
        {
            RechtBeschrijving = rechtBeschrijving;
            UitTeKerenToeslagBedrag = uitTeKerenToeslagBedrag;
            UitTeKerenToeslagBedragSpecified = uitTeKerenToeslagBedragSpecified;
        }

        public string RechtBeschrijving { get; }

        public decimal UitTeKerenToeslagBedrag { get; }

        public bool UitTeKerenToeslagBedragSpecified { get; }
    }
}
