﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace alef_graphql_client
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class IitOperation
        : IOperation<IIit>
    {
        public string Name => "iit";

        public IDocument Document => IitQuery.Default;

        public OperationKind Kind => OperationKind.Query;

        public Type ResultType => typeof(IIit);

        public Optional<global::alef_graphql_client.Invoer__aanvraag__iitInput> Invoer { get; set; }

        public IReadOnlyList<VariableValue> GetVariableValues()
        {
            var variables = new List<VariableValue>();

            if (Invoer.HasValue)
            {
                variables.Add(new VariableValue("invoer", "Invoer__aanvraag__iitInput", Invoer.Value));
            }

            return variables;
        }
    }
}
