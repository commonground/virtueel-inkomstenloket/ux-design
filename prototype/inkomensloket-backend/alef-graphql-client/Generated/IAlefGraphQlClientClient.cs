﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using StrawberryShake;

namespace alef_graphql_client
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial interface IAlefGraphQlClientClient
    {
        Task<IOperationResult<global::alef_graphql_client.IIit>> IitAsync(
            Optional<global::alef_graphql_client.Invoer__aanvraag__iitInput> invoer = default,
            CancellationToken cancellationToken = default);

        Task<IOperationResult<global::alef_graphql_client.IIit>> IitAsync(
            IitOperation operation,
            CancellationToken cancellationToken = default);
    }
}
