﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using StrawberryShake;

namespace alef_graphql_client
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class AlefGraphQlClientClient
        : IAlefGraphQlClientClient
    {
        private const string _clientName = "AlefGraphQlClientClient";

        private readonly global::StrawberryShake.IOperationExecutor _executor;

        public AlefGraphQlClientClient(global::StrawberryShake.IOperationExecutorPool executorPool)
        {
            _executor = executorPool.CreateExecutor(_clientName);
        }

        public global::System.Threading.Tasks.Task<global::StrawberryShake.IOperationResult<global::alef_graphql_client.IIit>> IitAsync(
            global::StrawberryShake.Optional<global::alef_graphql_client.Invoer__aanvraag__iitInput> invoer = default,
            global::System.Threading.CancellationToken cancellationToken = default)
        {

            return _executor.ExecuteAsync(
                new IitOperation { Invoer = invoer },
                cancellationToken);
        }

        public global::System.Threading.Tasks.Task<global::StrawberryShake.IOperationResult<global::alef_graphql_client.IIit>> IitAsync(
            IitOperation operation,
            global::System.Threading.CancellationToken cancellationToken = default)
        {
            if (operation is null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            return _executor.ExecuteAsync(operation, cancellationToken);
        }
    }
}
