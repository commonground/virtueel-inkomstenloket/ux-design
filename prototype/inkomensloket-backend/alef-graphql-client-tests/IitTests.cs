using System;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using alef_graphql_client;

namespace alef_graphql_client_tests
{
    public class UnitTest1
    {
        [Fact]
        public async void CanQueryStub()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddHttpClient(
                "AlefGraphQlClientClient",
                c => c.BaseAddress = new Uri("http://localhost:5080/graphql"));
            serviceCollection.AddAlefGraphQlClientClient();

            IServiceProvider services = serviceCollection.BuildServiceProvider();
            IAlefGraphQlClientClient client = services.GetRequiredService<IAlefGraphQlClientClient>(); 
            var input = new Invoer__aanvraag__iitInput()
            {
                Alleenstaand = false,
                AowLeeftijdBehaald = false,
                InkomenPerMaand = 1500,
                OuderDan21 = true,
                ThuiswonendeKinderen = false,
                Vermogen = 12000,
                Woonplaats = "Utrecht"
            };
           
            var result = await client.IitAsync(input);
            Assert.Equal("stub static data", result.Data.Request.RechtBeschrijving);
        }
    }
}
