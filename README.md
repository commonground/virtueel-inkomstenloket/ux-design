# Klantreis
## Huidige klantreis

![](Pictures/klantreis-VIL-oud.png)

## Nieuwe Klantreis door gebruik van Virtueel Inkomensloket

![](Pictures/klantreis-VIL-nieuw.png)

# Spoor 1

Bevat 3 experimenten van het Virtueel Inkomensloket

## Experiment I
Versimpelde aanvraag vanuit beleid en voor de doelgroep


- IIT regel is versimpeld voor makkelijke aanvraag
- Standaard gegevensdefinitie IIT (GBI en Financieel Paspoort)
- Max 4 simpele vragen zijn opgesteld die een eerste indicatie geven op het recht en de hoogte van de IIT
- Validatie bij de doelgroep leidt tot een uitgewerkt UX design (t/m opgehaalde data)

> ## Design Prototype
>
> - [versie 0.1](https://www.figma.com/file/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=5%3A0) - Demo 25 Nov 2020
> - [versie 0.2](https://www.figma.com/file/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=784%3A2) - Demo 13 Jan 2021
> - [versie 0.3](https://www.figma.com/proto/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=1542%3A4563&scaling=min-zoom) - Demo 2 Mar 2021
> - [versie 0.4](https://www.figma.com/proto/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=2388%3A11452&scaling=scale-down&page-id=2388%3A11113) - Demo tbd
> - [versie 0.5](https://www.figma.com/proto/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=2722%3A457&scaling=scale-down&page-id=2722%3A0) - Demo tbd
>
> ### Gebruikersinzichten
>
> - [Inzichten versie 0.1](https://miro.com/app/board/o9J_lal1hJg=/)
> - [Inzichten versie 0.2](https://miro.com/app/board/o9J_lWo9OV4=/)
> - [Inzichten versie 0.3](https://miro.com/app/board/o9J_lPDDGgo=/)
> - [Inzichten versie 0.4](https://miro.com/app/board/o9J_lMqKktM=/)
> 
> ### Kijk mee met de laatste designs
>
> In deze [Figma](https://www.figma.com/file/jHAKDhY6XogAADSZjaxefP/Mobiel?node-id=0%3A1) delen we de verschillende versies van de ontwerpen. De ‘page’ met het hoogste nummer is de laatste versie. Per versie is een changelog beschikbaar die laat zien wat is aangepast sinds de vorige versie. In elke versie staan geplaatste comments en antwoorden die betrekking hebben op die versie.


## Experiment II
De versimpelde aanvraag in een werkend prototype

- In een werkend prototype maken we gebruik van de 4 simpele vragen (indicatie op het recht)
- Inwoner kan eigen data ophalen mbv I-wize die relevant zijn voor IIT, in een werkend prototype
- In het prototype maken we gebruik van de standaard gegevens definitie

## Experiment III
De inwoner kan bepalen of zij/hij daadwerkelijk recht heeft op de IIT

- Regels voor het bepalen van recht op IIT worden ingevoerd in regelchecker van TWI
- Opgehaalde data wordt langs regelchecker gehaald
- Data en aanvraag wordt naar de validator (W&I back office) gestuurd