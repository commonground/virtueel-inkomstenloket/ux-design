# Individuele Inkomenstoeslag

Een inkomenstoeslag voor mensen die 3 jaar of langer achter elkaar een laag inkomen hebben.

Deze is [hier](https://pki.utrecht.nl/Loket/product/0c17f7cd409dc999eb351883a138ca3d) aan te vragen

Het BPMN model alhier is de beslisboom van deze aanvraag.