# Decison Relation Diagram

Deze folder bevat een DMN/XML versie van de beslistabel. Let vooral op de hitpolicy. Die zorgt er voor dat de eerst geldende regel wordt gepakt. Als geen van de regels 1 t/m 4 geld, dan krijg je regel 5. Daardoor kan de beslistabel ook zo simpel blijven.

![Beslistabel](beslismodel.png)

Voor AOW vaststellen zou je nog een aparte beslistabel kunnen opnemen waarbij op basis van tabel van belastingdienst kunnen vaststellen of AOW leeftijd is bereikt op basis van geboortedatum (in een app kan dit uiteraard automatisch worden gedaan op basis van GBA gegevens).
